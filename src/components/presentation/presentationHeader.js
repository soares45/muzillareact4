import React, {Component} from 'react';
import PresentationNav from './presentationNav';

class PresentationHeader extends Component {

  render() {
    return (
      <header id="gtco-header" className="gtco-cover" role="banner" style={{backgroundImage: 'url(images/page-principale/img_bg_1.jpg)'}}>
        <div className="gtco-container">
        <PresentationNav />
          <div className="row">
            <div className="col-md-8 col-md-offset-2 text-center">
              <div className="display-t">
                <div className="display-tc animate-box" data-animate-effect="fadeIn">
                  <h1>Muzilla</h1>
                </div>
              </div>
            </div>
          </div>
        </div>
      </header>
    );
  }
}

export default PresentationHeader;
