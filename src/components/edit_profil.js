import React, {Component} from 'react';
import { Field, reduxForm } from 'redux-form';
import { connect } from 'react-redux';
import { fetchUpdateProfile, convertirClient } from '../actions';
import swal from 'sweetalert2'

class EditProfil extends Component {

  componentDidMount() {
    window.closeMenu();
  }

  renderField(field){
    const { meta: { touched, error }} = field;
    return (
      <div className="input-group">
        <span className="input-group-addon">
          <i className="material-icons">{field.icon}</i>
        </span>
        <div className={`form-line ${error ? "style={{borderBottom: '2px solid #F44336'}}" : ''}`}>
          <input
            type={field.type}
            className="form-control"
            placeholder={field.label}
            autofocus={field.autofocus}
            name={field.name}
            style={ error ? {borderBottom: '2px solid #F44336'} : {}}
            {...field.input}
          />
        </div>
        <label id={`${field.name}-error`} className="error" for={field.name}>
          {touched ? error : ''}
        </label>
      </div>
    );
  }

  convertClient(id){
    swal({
      title: 'Êtes-vous sûr?',
      text: 'Cette modification est permanente!',
      type: 'warning',
      showCancelButton: true,
      confirmButtonText: 'Oui',
      cancelButtonText: 'Non'
    }).then(() => {
      this.props.convertirClient(id, () => {
        swal(
          'Bravo.',
          'Votre compte a été convertie!',
          'success'
        ).then(() => {
          window.location.reload();
          window.seDeconneter();
        });
      });
    }, function(dismiss) {
      // dismiss can be 'overlay', 'cancel', 'close', 'esc', 'timer'
      if (dismiss === 'cancel') {
      }
    })
  }

  onSubmit(values){
    swal({
      title: 'Êtes-vous sûr?',
      text: 'Les modifications seront permanentes!',
      type: 'warning',
      showCancelButton: true,
      confirmButtonText: 'Oui, enregister!',
      cancelButtonText: 'Non, annuler'
    }).then(() => {
      this.props.fetchUpdateProfile(values, () => {
        swal(
          'Success!',
          'Votre compte a été modifié.',
          'success'
        ).then(() => {
          this.props.history.push('/accueil');
        });
      });
    }, function(dismiss) {
      // dismiss can be 'overlay', 'cancel', 'close', 'esc', 'timer'
      if (dismiss === 'cancel') {
      }
    })
  }

  render() {
    const { handleSubmit } = this.props;
    var convertion_menu = '';
    if(this.props.user.type === 'client'){
      convertion_menu = (
        <ul className="header-dropdown m-r--5">
          <li className="dropdown">
            <a href="javascript:void(0);" className="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
              <i className="material-icons">more_vert</i>
            </a>
            <ul className="dropdown-menu pull-right">
              <li>
              <a onClick={() => this.convertClient(this.props.user.id)} href="javascript:void(0);">
                <i className="material-icons">supervisor_account</i>Aller Entreprise</a>
              </li>
            </ul>
          </li>
        </ul>
      );
    }
    return (
      <div className="container-fluid">
        <div className="row clearfix">
          <div className="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div className="card">
              <div className="header">
                <h2>Informations du profil</h2>
                {convertion_menu}
              </div>
              <div className="body">
                <form onSubmit={handleSubmit(this.onSubmit.bind(this))}>
                  <Field
                    label="Nom d'utilisateur"
                    icon="person"
                    type="text"
                    name="username"
                    component={this.renderField}
                  />
                  <Field
                    label="Adresse Email"
                    icon="email"
                    type="text"
                    name="email"
                    component={this.renderField}
                  />
                  <Field
                    label="Mot de passe"
                    icon="lock"
                    name="newPassword"
                    type="password"
                    component={this.renderField}
                  />
                  <Field
                    label="Mot de passe"
                    icon="lock"
                    name="newPasswordComfirme"
                    type="password"
                    component={this.renderField}
                  />
                  <input type="hidden" name="id" />
                  <button className="btn btn-primary waves-effect" type="submit">Modifier</button>
                </form>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

function validate(values){
  const errors = {};

  if (!values.username && values.username === '') {
    errors.username = "Entrer un nom d'utilisateur";
  }

  if (values.newPassword && !values.newPasswordComfirme) {
    errors.newPassword = "Entrer un mot de passe identique";
    errors.newPasswordComfirme = "Entrer un mot de passe identique";
  }
  if (!values.newPassword && values.newPasswordComfirme) {
    errors.newPassword = "Entrer un mot de passe identique";
    errors.newPasswordComfirme = "Entrer un mot de passe identique";
  }
  if (values.newPassword !== values.newPasswordComfirme) {
    errors.newPassword = "Entrer un mot de passe identique";
    errors.newPasswordComfirme = "Entrer un mot de passe identique";
  }

  if (!values.email && values.email === '') {
    errors.email = "Entrer un email";
  }

  if (values.email && !/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i.test(values.email)) {
    errors.email = "Entrer un email valide. Example : example@gmail.com";
  }

  return errors;
}

function mapStateToProps(state, ownProps){
  return ({
    initialValues: state.user,
    user: state.user
  });
}

EditProfil = reduxForm({
  validate,
  form: 'EditProfil'
})(EditProfil)

EditProfil = connect(mapStateToProps,{fetchUpdateProfile, convertirClient})(EditProfil)

export default EditProfil;
