import React, {Component} from 'react';
import { Field, reduxForm } from 'redux-form';
import { withRouter } from 'react-router-dom';
import { connect } from 'react-redux';
import { fetchLogin } from '../../actions';
import LoginOption from './loginOption';

class LoginCard extends Component {

  renderField(field){
    const { meta: { touched, error }} = field;
    return (
      <div className="input-group">
        <span className="input-group-addon">
          <i className="material-icons">{field.icon}</i>
        </span>
        <div className={`form-line ${error ? "style={{borderBottom: '2px solid #F44336'}}" : ''}`}>
          <input
            type={field.type}
            className="form-control"
            placeholder={field.label}
            autofocus={field.autofocus}
            name={field.name}
            style={ error ? {borderBottom: '2px solid #F44336'} : {}}
            {...field.input}
          />
        </div>
        <label id={`${field.name}-error`} className="error" for={field.name}>
          {touched ? error : ''}
        </label>
      </div>
    );
  }

  onSubmit(values){
    this.props.fetchLogin(values, () => {
        this.props.history.push('/accueil');
    });
  }

  render() {
    const { handleSubmit } = this.props;

    return (
      <div className="card">
        <div className="body">
          <form onSubmit={handleSubmit(this.onSubmit.bind(this))}>
            <div className="msg">Connexion à votre compte</div>
            <Field
              label="Nom d'utilisateur"
              icon="person"
              required="true"
              autofocus="true"
              type="text"
              name="username"
              component={this.renderField}
            />
            <Field
              label="Mot de passe"
              icon="lock"
              required="true"
              autofocus="false"
              name="password"
              type="password"
              component={this.renderField}
            />
            <div className="row">
              <div className="col-xs-7 p-t-5">
              </div>
              <div className="col-xs-5">
                <button type="submit" className="btn btn-block bg-pink waves-effect">Se connecter</button>
              </div>
            </div>
            <LoginOption />
          </form>
        </div>
      </div>
    );
  }
}

function validate(values){
  const errors = {};

  if (!values.username) {
    errors.username = "Entrer une nom d'utilisateur";
  }
  if (!values.password) {
    errors.password = "Entrer votre mot de passe";
  }

  return errors;
}

export default reduxForm({
  validate,
  form: 'LoginForm'
})(
  withRouter(connect(null, { fetchLogin }) (LoginCard))
);
