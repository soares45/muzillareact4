import React, {Component} from 'react';
import { Link } from 'react-router-dom';

class LoginOption extends Component {

  render() {
    return (
      <div className="row m-t-15 m-b--20">
        <div className="col-xs-4">
          <Link to="/inscription">
            S'inscrire!
          </Link>
        </div>
      </div>
    );
  }
}

export default LoginOption;
