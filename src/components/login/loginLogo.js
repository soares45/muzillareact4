import React, {Component} from 'react';
import { Link } from 'react-router-dom';

class LoginLogo extends Component {

  render() {
    return (
      <div className="logo">
        <Link to="/">
          <b>Muzilla</b>
        </Link>
        <small>Service de streaming musical - Muzilla</small>
      </div>
    );
  }
}

export default LoginLogo;
