import React, {Component} from 'react';
import { ROOT_URL } from '../../actions';
//import AudioPlayer from './player/AudioPlayer';
import CustomReactAudioPlayer from './custom-react-audio-player';

class PlayerAudio extends Component {
  shouldComponentUpdate(nextProps, nextState){
    return (this.props.musiques !== nextProps.musiques);
  }
  render(){
    var playlist =[];
    for(var idx in this.props.musiques){
      playlist.push(`${ROOT_URL}/file/files/${this.props.musiques[idx]}`);
    }
    return (
        <CustomReactAudioPlayer controls src={playlist} volume={1.0}/>
    );
  }
}

export default PlayerAudio;
