import React, {Component} from 'react';
import PlayerAudio from './player_audio';
import { connect } from 'react-redux';

class Holder extends Component {
  render() {
    return (
      <div>
      <li className="header">LECTEUR AUDIO</li>
      <li><PlayerAudio musiques={this.props.playerPlaylist} /></li>
      </div>
    );
  }
}

function mapStateToProps({playerPlaylist}){
  return {playerPlaylist};
}

export default connect(mapStateToProps)(Holder);
