import React, {Component} from 'react';
import { Link } from 'react-router-dom';

class TopBar extends Component {

  handleParentClick(e) {
    window.fadeOut();
  }

  handleChildClick(e) {
    e.stopPropagation();
  }

  render() {
    return (
      <nav className="navbar" style={{backgroundColor: '#F44336'}} onClick={this.handleParentClick}>
        <div className="container-fluid">
          <div className="navbar-header">
            <a className="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar-collapse" aria-expanded="false" />
            <a href="javascript:void(0);" className="bars" style={{color: '#fff'}} onClick={this.handleChildClick}/>
            <Link className="navbar-brand" to="/accueil" style={{marginLeft: '28px', color: '#fff'}}>Muzilla - Service de streaming musical</Link>
          </div>
          <div className="collapse navbar-collapse" id="navbar-collapse">
            <ul className="nav navbar-nav navbar-right">
              <li><a href="javascript:void(0);" className="js-search" data-close="true" style={{color: '#fff'}}><i className="material-icons">search</i></a></li>
            </ul>
          </div>
        </div>
      </nav>
    );
  }
}

export default TopBar;
