import React, {Component} from 'react';
import UserInfo from './sideBar/userInfo';
import Footer from './sideBar/footer';
import Menu from './sideBar/menu';

class SideBar extends Component {

  render() {
    return (
      <section>
        <aside id="leftsidebar" className="sidebar">
          <UserInfo />
          <Menu />
          <Footer />
        </aside>
      </section>
    );
  }
}

export default SideBar;
