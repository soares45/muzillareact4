import React, {Component} from 'react';
import NavigationPrincipale from './menu/navigationPrincipale';
import Bibiotheque from './menu/bibiotheque';
import Holder from '../../audio_player/holder';
class Footer extends Component {

  render() {
    return (
      <div className="menu">
        <ul className="list">
          <NavigationPrincipale />
          <Holder />
          <Bibiotheque />
        </ul>
      </div>
    );
  }
}

export default Footer;
