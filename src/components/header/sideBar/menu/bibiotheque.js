import React, {Component} from 'react';
import Playlist from './playlist';
import AddPlaylist from './add_playlist';

class Bibiotheque extends Component {

  render() {
    return (
      <div>
        <AddPlaylist />
        <li className="header">BIBIOTHÈQUE</li>
        <Playlist />
      </div>
    );
  }
}

export default Bibiotheque;
