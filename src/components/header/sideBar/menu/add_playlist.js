import React, { Component } from 'react';
import { connect } from 'react-redux';
import { ajoutPlaylist } from '../../../../actions';
import { Field, reduxForm, reset } from 'redux-form';
import swal from 'sweetalert2';

class AddPlaylist extends Component {
  renderField(field){
    const { meta: { touched, error }} = field;
    return (
      <div className="input-group">
        <span className="input-group-addon">
          <i className="material-icons">{field.icon}</i>
        </span>
        <div className={`form-line ${error ? "style={{borderBottom: '2px solid #F44336'}}" : ''}`}>
          <input required
            type={field.type}
            className="form-control"
            placeholder={field.label}
            autofocus={field.autofocus}
            name={field.name}
            style={ error ? {borderBottom: '2px solid #F44336'} : {}}
            {...field.input}
          />
        </div>
        <label id={`${field.name}-error`} className="error" for={field.name}>
          {touched ? error : ''}
        </label>
      </div>
    );
  }

  onSubmit(values){
    if(values.titre !== "Historique" && values.titre !== "Favoris"){
      swal({
        title: 'Création de playlist',
        text: 'Êtes-vous sûr de vouloir créer cette playlist?',
        type: 'warning',
        showCancelButton: true,
        confirmButtonText: 'Oui, créer!',
        cancelButtonText: 'Non, annuler'
      }).then(() => {
        this.props.ajoutPlaylist(values, this.props.user.id, () => {
          swal(
            'Success!',
            'Votre playlist a été créée.',
            'success'
          );
        });
      }, function(dismiss) {
        if (dismiss === 'cancel') {
        }
      });
    }else{
      swal({
        title: 'Playlist unique',
        text: 'Désolé, ses playlists ne peuvent pas être dupliquées',
        type: 'warning',
      })
    }
  }

  render() {
    const { handleSubmit } = this.props;
    return (
      <div>
        <form onSubmit={handleSubmit(this.onSubmit.bind(this))} >
          <li className="header">+PLAYLIST</li>
          <Field
            label="Nouvelle playlist"
            icon="library_music"
            type="text"
            name="titre"
            component={this.renderField}
          />
          <button className="btn btn-primary waves-effect" type="submit">Ajouter la playlist</button>
        </form>
      </div>
    )
  }
}

function validate(values){
  const errors = {};

  return errors;
}

function mapStateToProps({user}){
  return { user };
}
const afterSubmit = (result, dispatch) => dispatch(reset('ajoutPlaylist'));

AddPlaylist = reduxForm({
  validate,
  onSubmitSuccess: afterSubmit,
  form: 'ajoutPlaylist'
})(AddPlaylist)

export default connect(mapStateToProps, {ajoutPlaylist})(AddPlaylist);
