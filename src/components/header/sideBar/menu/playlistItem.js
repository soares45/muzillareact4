import React, {Component} from 'react';
import MusiqueList from './musiqueList';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';

var icon;
class PlaylistItem extends Component {

  componentWillMount(){
    console.log(this.props.titre);
    if(this.props.titre === "Historique"){
      icon = "history";
    } else if (this.props.titre === "Favoris"){
      icon = "favorite";
    } else {
      icon = "library_music";
    }
  }


  render() {
    return (
      <div>
        <a href="javascript:void(0);" className="menu-toggle">
          <i className="material-icons">{icon}</i>
          <span>{this.props.titre}</span>
        </a>
        <ul className="ml-menu">
          <li>
            <Link to={`/playlist/${this.props.id}`}>
              <span>Voir {this.props.titre}</span>
            </Link>
          </li>
          <MusiqueList playlistId={this.props.playlistId} />
        </ul>
      </div>
    );
  }
}

function mapStateToProps(state, ownProps){
  return state.playlists[ownProps.playlistId];
}

export default connect(mapStateToProps)(PlaylistItem);
