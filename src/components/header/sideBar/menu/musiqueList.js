import React, {Component} from 'react';
import { connect } from 'react-redux';
import { fetchLoginMusique } from '../../../../actions';
import { Link } from 'react-router-dom';
import _ from 'lodash'; 

class MusiqueList extends Component {

  componentDidMount(){
    this.props.fetchLoginMusique(this.props);
  }

  renderMusiqueItems(){
    return _.map(this.props.musiques, musique => {
      return (
        <li key={musique.id}>
        <Link to={`/musique/${musique.id}`}>
          <span>{musique.titre}</span>
        </Link>
        </li>
      );
    });
  }

  render() {
    return (
      <div>
        {this.renderMusiqueItems()}
      </div>
    );
  }
}

function mapStateToProps(state, ownProps){
  const musiques = {};
  if (typeof state.musiques[''+ownProps.playlistId] !== 'undefined') {
    musiques.musiques = state.musiques[''+ownProps.playlistId];
    return musiques;
  }
  return state.musiques;
}

export default connect(mapStateToProps, {fetchLoginMusique})(MusiqueList);
