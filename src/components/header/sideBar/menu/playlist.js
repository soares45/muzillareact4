import React, { Component } from 'react';
import { connect } from 'react-redux';
import _ from 'lodash';
import PlaylistItem from './playlistItem';


class Playlist extends Component {

  renderPlaylistItems(){
    return _.map(this.props.playlists, playlist => {
      return (
        <li key={playlist.id}>
          <PlaylistItem playlistId={playlist.id} />
        </li>
      );
    });
  }

  render() {
    return (
      <div>
        {this.renderPlaylistItems()}
      </div>
    )
  }
}

function mapStateToProps({playlists}){
  return { playlists };
}

export default connect(mapStateToProps)(Playlist);
