import React, {Component} from 'react';
import { Link } from 'react-router-dom';

import { connect } from 'react-redux';

class NavigationPrincipale extends Component {

  render() {
    var ajoutMusique_menu = '';
    var entrepriseMusiques = '';
    if(this.props.user.type === 'entreprise'){
      ajoutMusique_menu = (
        <li>
          <Link to="/ajoutMusique">
            <i className="material-icons">library_add</i>
            <span>Ajouter une musique</span>
          </Link>
        </li>
      );
      entrepriseMusiques = ( <li>
          <Link to="/entrepriseMusiques">
            <i className="material-icons">queue_music</i>
            <span>Consulter mes musiques</span>
          </Link>
        </li>
      );
    }
    return (
      <div>
        <li className="header">NAVIGATION PRINCIPALE</li>
        <li>
          <Link to="/accueil">
            <i className="material-icons">home</i>
            <span>Accueil</span>
          </Link>
        </li>
        <li>
          <Link to="/profile">
            <i className="material-icons">person</i>
            <span>Profile</span>
          </Link>
        </li>
        {ajoutMusique_menu}
        {entrepriseMusiques}
      </div>
    );
  }
}

function mapStateToProps({user}){
  return {user};
}

export default connect(mapStateToProps)(NavigationPrincipale);
