import React, {Component} from 'react';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';

class UserInfo extends Component {

  render() {
    const { user } = this.props;
    return (
      <div className="user-info">
        <div className="image">
          <img src="images/user.png" width={48} height={48} alt="User" />
        </div>
        <div className="info-container">
          <div className="name" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">{user.username}</div>
          <div className="email">{user.email}</div>
          <div className="btn-group user-helper-dropdown">
            <i className="material-icons" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">keyboard_arrow_down</i>
            <ul className="dropdown-menu pull-right">
              <li><Link to="/profile"><i className="material-icons">person</i>Profile</Link></li>
              <li role="seperator" className="divider" />
              <li><a href="/" id="seDeconneter"><i className="material-icons">input</i>Déconnexion</a></li>
            </ul>
          </div>
        </div>
      </div>
    );
  }
}

function mapStateToProps({user}){
  return {user};
}

export default connect(mapStateToProps)(UserInfo);
