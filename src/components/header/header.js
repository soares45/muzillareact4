import React, {Component} from 'react';
import Loader from './loader';
import CustomSearchBar from './CustomSearchBar';
import TopBar from './topBar';
import SideBar from './sideBar';

class Header extends Component {

  render() {
    return (
      <div>
        <Loader />
        <div className="overlay" />
        <CustomSearchBar />
        <TopBar />
        <SideBar />
      </div>
    );
  }
}

export default Header;
