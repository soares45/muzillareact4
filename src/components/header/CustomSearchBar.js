import React, {Component} from 'react';
import {withRouter} from 'react-router-dom';
import { connect } from 'react-redux';
import { fetchRecherche, clearRecherche } from '../../actions';
import SearchBar from 'material-ui-search-bar';
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';

class CustomSearchBar extends Component {

    afficherMusique(musiqueId){
        window.rechecheClose();
        this.props.history.push(`/musique/${musiqueId}`);
    }

    afficherPlaylist(playlistId){
      window.rechecheClose();
      this.props.history.push(`/playlist/${playlistId}`);
    }

    renderResultats() {
      const containerStyle ={
      };
      const divStyle = {
        backgroundColor: 'white',
      };
      const {resultats}  = this.props;
      return (
        <div style={containerStyle}>{
          resultats.map(resultat => {
           if(resultat.duree !== undefined){
             return (
               <div style={divStyle} key={`M${resultat.id}`}>
               <a onClick={() => this.afficherMusique(resultat.id)} href="javascript:void(0);">
                <i className="material-icons">music_note</i>
                {resultat.titre}<small> par </small>{resultat.artiste}
              </a>
              <br />
              </div>
              );
            }
            else{
              return (
                <div style={divStyle} key={`P${resultat.id}`}>
                <a onClick={() => this.afficherPlaylist(resultat.id)} href="javascript:void(0);">
                 <i className="material-icons">queue_music</i>
                 {resultat.titre}
                </a>
                <br />
                </div>
              );
            }
          })
        }</div>
      );
    }
    valueChange(value){
      if(value !== "")
        this.props.fetchRecherche(value);
      else
        this.props.clearRecherche();
    }
    render() {
      return (
        <div className="search-bar">
          <div className="search-icon">
            <i className="material-icons">search</i>
          </div>
          <MuiThemeProvider>
            <SearchBar
              onChange={(value) => this.valueChange(value)}
              onRequestSearch={() => null}
              closeIcon={null}
              searchIcon={null}
              hintText={""}
              style={{
                margin: '0 auto',
                height: 70,
                paddingLeft: 20
              }}
            />
          </MuiThemeProvider>
          {this.renderResultats()}
          <div className="close-search">
            <i className="material-icons">close</i>
          </div>
        </div>
      );
    }
  }

function mapStateToProps({ resultats }){
  return { resultats };
}

export default withRouter(connect(mapStateToProps, { fetchRecherche, clearRecherche } )(CustomSearchBar));
