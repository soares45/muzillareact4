import React, {Component} from 'react';
import MusiqueList from './musiqueList';
import { connect } from 'react-redux';
import { fetchMusiqueAleatoire } from '../../actions';

class MusiquePopulaireBody extends Component {

  componentDidMount(){
    this.props.fetchMusiqueAleatoire();
  }

  render() {
    return (
      <div className="body">
        <MusiqueList />
      </div>
    );
  }
}

export default connect(null, {fetchMusiqueAleatoire})(MusiquePopulaireBody);
