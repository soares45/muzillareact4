import React, {Component} from 'react';

class MusiquePopulaireHeader extends Component {

  render() {
    return (
      <div className="header">
        <h2>
          Musique Aleatoire
        </h2>
      </div>
    );
  }
}

export default MusiquePopulaireHeader;
