import React, {Component} from 'react';
import { connect } from 'react-redux';
import { setPlayerPlaylist } from '../../actions';
import _ from 'lodash';

class MusiqueList extends Component {

  renderMusiqueItems(musiques){
    console.log(musiques);
    return _.map(musiques, (musique) => {
      var musique_playlist = [];
      musique_playlist.push(musique);
      return (
        <div className="col-sm-6 col-md-3" key={musique.id}>
          <div className="thumbnail">
            <div className="caption">
              <h3>{musique.titre} <a onClick={() => this.props.setPlayerPlaylist(musique_playlist)} href="javascript:void(0);" className="btn btn-primary waves-effect right" role="button">&Eacute;couter</a><br/>
                <small>Par {musique.artiste}</small>
              </h3>
              <hr />
              <p>
                Boite de production: max
              </p>
            </div>
          </div>
        </div>
      );
    });
  }

  render() {
    return (
      <div className="row">
        {this.renderMusiqueItems(this.props.musiquesAleatoires)}
      </div>
    );
  }
}

function mapStateToProps({musiquesAleatoires}){
  return {musiquesAleatoires};
}

export default connect(mapStateToProps, { setPlayerPlaylist })(MusiqueList);
