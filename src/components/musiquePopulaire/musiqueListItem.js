import React, {Component} from 'react';
import { connect } from 'react-redux';
import { setPlayerPlaylist } from '../../actions';

class MusiqueListItem extends Component {

  render() {
    const {musiqueActive} = this.props;
    var musique_playlist = [];
    musique_playlist.push(musiqueActive);
    return (
      <div className="col-sm-6 col-md-3">
        <div className="thumbnail">
          <div className="caption">
            <h3>Musique #1 <a onClick={() => this.props.setPlayerPlaylist(musique_playlist)} href="javascript:void(0);" className="btn btn-primary waves-effect right" role="button">&Eacute;couter</a><br/>
              <small>Par Alex</small>
            </h3>
            <hr />
            <p>
              Boite de production: max
            </p>
          </div>
        </div>
      </div>
    );
  }
}

function mapStateToProps({ musiqueActive }){
  return { musiqueActive };
}

export default connect(mapStateToProps, { setPlayerPlaylist} )(MusiqueListItem);
