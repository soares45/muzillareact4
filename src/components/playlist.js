import React, {Component} from 'react';
import {connect} from 'react-redux';
import {fetchMusiqueByPlaylistId, fetchPlaylist, updatePlaylist, supprimerPlaylist, clonePlaylist, fetchLoginMusique, fetchLoginPlaylist, retireMusique, setPlayerPlaylist} from '../actions';
import {Link} from 'react-router-dom';
import _ from 'lodash';
import swal from 'sweetalert2';

class Playlist extends Component {

  componentWillReceiveProps(nextProps){
    window.closeMenu();
    const nextId = nextProps.match.params.id;
    const prevId = this.props.match.params.id;
    if( nextId !== prevId ){
      const { id } = nextProps.match.params;
      this.props.fetchPlaylist(id);
      this.props.fetchMusiqueByPlaylistId(id);
    }
  }

  componentDidMount() {
    window.closeMenu();
    const {id} = this.props.match.params;
    this.props.fetchPlaylist(id);
    this.props.fetchMusiqueByPlaylistId(id);
  }

  renderPlaylistItem(musique) {
    const {playlistActif} = this.props;
    return (
      <div className="media">
        <div className="media-left">
          <Link to={`/musique/${musique["id"]}`}>
            <img className="media-object" src="http://placehold.it/64x64" width="64" height="64"/>
          </Link>
        </div>
        <div className="media-body media-middle">
          <Link to={`/musique/${musique["id"]}`}>
            <h4 className="media-heading">{musique["titre"]}</h4>
          </Link>
          <p>
            <small>{`par ${musique["artiste"]}`}</small>
          </p>
        </div>
        <div className="media-right">
          <ul className="header-dropdown m-r--5">
            <li className="dropdown">
              <a href="javascript:void(0);" className="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="true">
                <i className="material-icons">more_vert</i>
              </a>
              <ul className="dropdown-menu pull-right">
                <li>
                  <Link to={`/musique/${musique["id"]}`}>
                    <i className="material-icons">play_arrow</i>&Eacute;couter
                  </Link>
                </li>
                <li>
                  <a onClick={() => this.props.retireMusique(musique.id, playlistActif.id, () => {this.props.history.push(`/accueil`);})} href="javascript:void(0);">
                    <i className="material-icons">delete</i>Retirer
                  </a>
                </li>
              </ul>
            </li>
          </ul>
        </div>
      </div>
      );
    }

    renderMusiques() {
      const {musiquesActives} = this.props;
      return _.map(musiquesActives, musique => {
        return (<div key={musique.id} className="media">
          {this.renderPlaylistItem(musique)}
        </div>);
      });
    }

    modifierPlaylist(titrePlaylist, idPlaylist) {
      const variables = {};
      variables.id = idPlaylist;
      variables.visible = true;

      swal({
        title: 'Mettre le nouveau titre pour la playlist',
        input: 'text',
        inputValue: titrePlaylist,
        showCancelButton: true,
        confirmButtonText: 'Soumettre',
        cancelButtonText: 'Annuler',
        showLoaderOnConfirm: true,
        allowOutsideClick: true
      }).then((nouveauTitre) =>  {
          variables.titre = nouveauTitre;
          if(nouveauTitre !== ""){
            this.props.updatePlaylist(variables,this.props.user.id, () => {
              this.props.fetchPlaylist(this.props.playlistActif.id);
            });
            swal({
              type: 'success',
              title: 'Votre nom de playlist a été modifié!',
              html: 'Nouveau nom: '+ nouveauTitre
            });
          }else{
            swal({
              type: 'error',
              title: 'Veuillez donner un nom à votre playlist'
            });
          }
      });
    }

    copierPlaylist(titrePlaylist, idPlaylist) {
      const variables = {};
      variables.id = idPlaylist;
      variables.visible = true;

      swal({
        type: 'question',
        title: 'Voulez vous renommer la playlist à copier?',
        input: 'text',
        inputValue: titrePlaylist,
        showCancelButton: true,
        confirmButtonText: 'Soumettre',
        cancelButtonText: 'Annuler',
        showLoaderOnConfirm: true,
        allowOutsideClick: true
      }).then((nouveauTitre) =>  {
          variables.titre = nouveauTitre;
          if(nouveauTitre !== ""){
            this.props.clonePlaylist(variables,this.props.user.id, () => {
              this.props.history.push(`/accueil`);
            });
            swal({
              type: 'success',
              title: 'Cette playlist a été copiée!',
              html: 'Nouveau nom: '+ nouveauTitre
            });
          }else{
            swal({
              type: 'error',
              title: 'Veuillez donner un nom à votre playlist'
            });
          }
      });
    }

    supprimerPlaylist(idPlaylist){
      swal({
        title: 'Êtes-vous sûr de vouloir supprimer votre playlist?',
        showCancelButton: true,
        type: 'warning',
        confirmButtonText: 'Oui',
        cancelButtonText: 'Non',
        showLoaderOnConfirm: true,
        allowOutsideClick: true
      }).then(() =>  {
        this.props.supprimerPlaylist(idPlaylist, () => {


          this.props.history.push('/accueil');
        });
        swal({
          type: 'success',
          title: 'Votre playlist a été supprimée!'
        });
      });
    }

    render() {
      const {musiquesActives, playlistActif} = this.props;
      if (typeof musiquesActives === 'undefined' || typeof playlistActif === 'undefined') {
        return (<div>Loading</div>);
      }
      var playlistVisibility = "";
      if (playlistActif["visible"]) {
        playlistVisibility = "Cette playlist est visible.";
      } else {
        playlistVisibility = "Cette playlist n'est pas visible.";
      }
      var historiqueFavorisDisplay = "";
      if(playlistActif.titre === "Historique" || playlistActif.titre === "Favoris"){
        historiqueFavorisDisplay = "none"
      }else{
        historiqueFavorisDisplay = "";
      }
      var editPlaylist_menu = '';
      var supprimerPlaylist_menu = '';
      if(this.props.user.id === this.props.playlistActif.clientId){
        editPlaylist_menu = (
          <li>
            <a style={{display:historiqueFavorisDisplay}} onClick={() => this.modifierPlaylist(playlistActif.titre, playlistActif.id)} href="javascript:void(0);">
              <i className="material-icons">mode_edit</i>Modifier
            </a>
          </li>
        );
        supprimerPlaylist_menu = (
          <li>
            <a style={{display:historiqueFavorisDisplay}} onClick={() => this.supprimerPlaylist(playlistActif.id)} href="javascript:void(0);">
              <i className="material-icons">delete</i>Supprimer</a>
          </li>
        );
      }
      var musique_playlist = [];
      _.map(musiquesActives, musique => {(
          musique_playlist.push(musique)
      )});

      return (<div className="row clearfix">
        <div className="col-lg-12 col-md-12 col-sm-12 col-xs-12">
          <div className="card">
            <div className="header">
              <h2>
                {playlistActif.titre}<br/>
                <small>{playlistVisibility}</small>
              </h2>
              <ul className="header-dropdown m-r--5">
                <li className="dropdown">
                  <a href="javascript:void(0);" className="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                    <i className="material-icons">more_vert</i>
                  </a>
                  <ul className="dropdown-menu pull-right">
                    <li>
                      <a onClick={() => this.props.setPlayerPlaylist(musique_playlist)} href="javascript:void(0);">
                        <i className="material-icons">play_arrow</i>&Eacute;couter</a>
                    </li>
                    {editPlaylist_menu}
                    <li>
                    <a  onClick={() => this.copierPlaylist(playlistActif.titre, playlistActif.id)} href="javascript:void(0);">
                      <i className="material-icons">content_copy</i>Cloner
                    </a>
                    </li>
                    {supprimerPlaylist_menu}
                  </ul>
                </li>
              </ul>
            </div>
            <div className="body">
              <div className="bs-example" data-example-id="media-alignment">
                {this.renderMusiques()}
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

function mapStateToProps({playlistActif, musiquesActives, user, playlists}) {
  return {playlistActif, musiquesActives, user, playlists};
}

export default connect(mapStateToProps, {fetchPlaylist, fetchMusiqueByPlaylistId, updatePlaylist, supprimerPlaylist , clonePlaylist, fetchLoginMusique, fetchLoginPlaylist, retireMusique, setPlayerPlaylist})(Playlist);
