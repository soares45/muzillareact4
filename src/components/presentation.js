import React, {Component} from 'react';
import PresentationHeader from './presentation/presentationHeader';
import PresentationDescription from './presentation/presentationDescription';
import PresentationNos from './presentation/presentationNos';

class Presentation extends Component {
  render() {
    return (
      <div>
        <PresentationHeader />
        <PresentationDescription />
        <PresentationNos />
      </div>
    );
  }
}

export default Presentation;
