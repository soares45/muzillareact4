import React, {Component} from 'react';
import { connect } from 'react-redux';
import {Route, Switch} from 'react-router-dom';
import Header from './header/header';
import ScripJS from './utils/load_scrips';
import { fetchLoginPlaylist } from '../actions';
import EditProfil from './edit_profil';
import Playlist from './playlist';
import MusiquePopulaire from './musiquePopulaire';
import AfficheMusique from './musique/affiche_musique';
import AjoutMusique from './musique/ajout_musique';
import EditMusique from './musique/edit_musique';
import EntrepriseMusiques from './musique/entreprise_musiques';

class Accueil extends Component {

  componentWillMount (){
    const { user } = this.props;
    this.props.fetchLoginPlaylist(user);
    window.test();
  }


  render() {
    return (
      <div>
        <ScripJS />
        <Header />
        <section className="content">
          <div className="container-fluid">
            <Switch>
              <Route path="/accueil" component={MusiquePopulaire} />
              <Route path="/profile" component={EditProfil} />
              <Route path="/playlist/:id" component={Playlist} />
              <Route path="/musique/:id" component={AfficheMusique} />
              <Route path="/ajoutMusique" component={AjoutMusique} />
              <Route path="/editMusique" component={EditMusique} />
              <Route path="/entrepriseMusiques" component={EntrepriseMusiques} />
            </Switch>
          </div>
        </section>
      </div>
    );
  }
}

function mapStateToProps({user}){
  return {user};
}

export default connect(mapStateToProps, {fetchLoginPlaylist})(Accueil);
