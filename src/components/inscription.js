import React, {Component} from 'react';
import InscriptionLogo from './inscription/inscriptionLogo';
import InscriptionCard from './inscription/inscriptionCard';

class Inscription extends Component {

  render() {
    return (
      <div className="signup-page">
        <div className="signup-box">
          <InscriptionLogo />
          <InscriptionCard />
        </div>
      </div>
    );
  }
}

export default Inscription;
