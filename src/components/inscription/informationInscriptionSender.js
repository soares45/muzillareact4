import 'whatwg-fetch'
import swal from 'sweetalert2'
import {ROOT_URL} from '../../actions';

//G est que la variable a ete verifier et est pretre a etre soumise dans la base de donnees
var usernameG;
var passwordG;
var passwordG2;
var emailG;
var passwordCorrect = false;
var allInformationsCorrect = false;
var usernameExiste = true;
var emailIsOk = false;

class informationInscriptionSender {
  static setUserName(username) {
    usernameG = username;
  }

  static setPassword(password) {
    passwordG = password;
  }

  static checkPasswordInitiale(password) {
    if (password !== passwordG2) {
      passwordCorrect = false;
    } else {
      passwordCorrect = true;
    }
    return passwordCorrect;
  }

  static checkPassword(password2) {
    passwordG2 = password2;
    if (passwordG !== passwordG2) {
      passwordCorrect = false;
    } else {
      passwordCorrect = true;
    }
    return passwordCorrect;
  }

  static setEmail(email) {
    emailG = email;
  }

  static setUsernameExiste(existe) {
    existe
      ? usernameExiste = true
      : usernameExiste = false;
  }

  static checkEmailIsOk() {
    emailIsOk = true;
  }

  static checkEmailIsNotOk() {
    emailIsOk = false;
  }

  static checkAllInformationsCorrect() {
    if (passwordCorrect && emailIsOk && !usernameExiste && passwordG !== "") {
      allInformationsCorrect = true;
    } else {
      allInformationsCorrect = false;
    }
  }

  static createClient() {
    function checkStatus(response) {
      if (response.status >= 200 && response.status < 300) {
        return response
      } else {
        var error = new Error(response.statusText)
        error.response = response
        throw error
      }
    }

    this.checkAllInformationsCorrect();
    if (allInformationsCorrect) {
      fetch(`${ROOT_URL}/client/createClient`, {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json'
        },
        body: JSON.stringify({username: usernameG, password: passwordG, email: emailG})
      }).then(checkStatus).then(function(data) {
        swal({type: 'success', title: 'Votre compte a été créé', showConfirmButton: false, timer: 1500}).then(function() {},
        // handling the promise rejection
        function(dismiss) {
          if (dismiss === 'timer') {
            window.location.replace("/");
          }
        })
      }).catch(function(error) {
          swal('Oops...', 'L\'email existe déjà. Avez vous oubliez votre compte?', 'error');
      })
    }else if (usernameExiste) {
      swal('Oops...', 'Veuillez s\'il vous plait changer votre votre nom d\'utilisateur, car il existe déjà!', 'error');
    } else if (!emailIsOk) {
      swal('Oops...', 'L\'email n\'est pas correct, veuillez s\'il vous plait le changer.', 'error');
    }else if(!passwordCorrect){
      swal('Oops...', 'Votre mot de passe ne correspond pas dans les deux champs', 'error');
    }else if(passwordG === ""){
      swal('Oops...', 'Veuillez mettre un mot de passe', 'error');
    }
  }
}

export default informationInscriptionSender;
