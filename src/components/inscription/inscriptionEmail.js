import React, {Component} from 'react';
import inscriptionSender from './informationInscriptionSender';

class InscriptionEmail extends Component {

  constructor() {
    super();
    this.state = {
      color: ''
    };
    this.setEmail = this.setEmail.bind(this);
  }

  setEmail(email) {
    var emailVerification = email.target.value;
    inscriptionSender.setEmail(emailVerification);
    if (/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test(emailVerification)) {
      this.setState({color: ''});
      inscriptionSender.checkEmailIsOk();
    }else{
      inscriptionSender.checkEmailIsNotOk();
      this.setState({color: '#FFC0CB'});
    }
  }

  render() {
    return (<div className="input-group">
      <span className="input-group-addon">
        <i className="material-icons">email</i>
      </span>
      <div className="form-line">
        <input style={{ backgroundColor: this.state.color }} type="email" className="form-control" name="email" placeholder="Votre adresse Email" required="required" onChange={this.setEmail}/>
      </div>
    </div>);
  }
}

export default InscriptionEmail;
