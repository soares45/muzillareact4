import React, {Component} from 'react';
import inscriptionSender from './informationInscriptionSender';

class InscriptionPassword extends Component {

  setPassword(password){
    inscriptionSender.setPassword(password.target.value);
    inscriptionSender.checkPasswordInitiale(password.target.value);

  }

  render() {
    return (
      <div className="input-group">
        <span className="input-group-addon">
          <i className="material-icons">lock</i>
        </span>
        <div className="form-line">
          <input type="password" className="form-control" name="password" minLength={6} placeholder="Mot de passe" required onChange={this.setPassword} />
        </div>
      </div>
    );
  }
}

export default InscriptionPassword;
