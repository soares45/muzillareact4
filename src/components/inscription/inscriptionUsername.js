import React, {Component} from 'react';
import inscriptionSender from './informationInscriptionSender';
import axios from 'axios';
import { ROOT_URL } from '../../actions';

var styles = {
  color: '#ff748c',
  fontWeight: 'bold'
};

class InscriptionUsername extends Component {
  constructor() {
    super();
    this.state = {
      color: '',
      messageErreur: ''
    }; 
  }

  setUserName(username) {
    var usernameEnCoursCreation = username.target.value;
    inscriptionSender.setUserName(usernameEnCoursCreation);
    if (usernameEnCoursCreation !== '') {
      axios.get(`${ROOT_URL}/client//findByUsername/` + usernameEnCoursCreation).then((response) => {
        if (response.data) {
          this.setState({color: '#FFC0CB', messageErreur: 'Le nom d\'utilisateur est pris'});
          inscriptionSender.setUsernameExiste(true);
        } else {
          this.setState({color: '', messageErreur: ' '});
          inscriptionSender.setUsernameExiste(false);
        }
      })
    } else {
      this.setState({color: ''});
    }
  }

  render() {
    return (<div className="input-group">
      <span className="input-group-addon">
        <i className="material-icons">person</i>
      </span>
      <div className="form-line">
        <input style={{
            backgroundColor: this.state.color
        }} type="text" className="form-control" name="namesurname" placeholder="Nom d'utilisateur" required="required" autofocus="autofocus" onChange={event => this.setUserName(event)}/>

      </div>
      <span style={styles}>{this.state.messageErreur}</span>
    </div>);
  }
}

export default InscriptionUsername;
