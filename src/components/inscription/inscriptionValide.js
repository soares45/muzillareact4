import React, {Component} from 'react';
import inscriptionSender from './informationInscriptionSender';

class InscriptionValide extends Component {

  render() {
    return (<div className="form-group">
      <input required="required" type="checkbox" name="terms" id="terms" className="filled-in chk-col-pink"/>
      <label onClick={inscriptionSender.setTerms} htmlFor="terms">J&acute;accepte les termes</label>
    </div>);
  }
}

export default InscriptionValide;
