import React, {Component} from 'react';
import InscriptionUsername from './inscriptionUsername';
import InscriptionEmail from './inscriptionEmail';
import InscriptionPassword from './inscriptionPassword';
import InscriptionPasswordConfirmer from './inscriptionPasswordConfirmer';
import InscriptionValide from './inscriptionValide';
import InscriptionOption from './inscriptionOption';
import inscriptionSender from './informationInscriptionSender';

class InscriptionCard extends Component {

  createClient(e) {
    e.preventDefault();
    inscriptionSender.createClient();
  }

  render() {
    return (<div className="card">
      <div className="body">
        <form>
          <div className="msg">Enregistrez un nouveau compte</div>
          <InscriptionUsername/>
          <InscriptionEmail/>
          <InscriptionPassword/>
          <InscriptionPasswordConfirmer/>
          <button onClick={this.createClient} type="submit" className="btn btn-block btn-lg bg-pink waves-effect">S&acute;inscrire!</button>
          <InscriptionOption/>
        </form>
      </div>
    </div>);
  }
}

export default InscriptionCard;
