import React, {Component} from 'react';
import { Link } from 'react-router-dom';

class InscriptionOption extends Component {

  render() {
    return (
      <div>
        <div className="m-t-25 m-b--5 align-center">
          <Link to="/connection">
            Vous avez déjà un compte?
          </Link>
        </div>
      </div>
    );
  }
}

export default InscriptionOption;
