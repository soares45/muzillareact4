import React, {Component} from 'react';
import inscriptionSender from './informationInscriptionSender';

var isPasswordMathing = true;
class InscriptionPasswordConfirmer extends Component {
  constructor() {
    super();
    this.state = {
      color: ''
    };
    this.checkPassword = this.checkPassword.bind(this);
  }

  checkPassword(password) {
    isPasswordMathing = inscriptionSender.checkPassword(password.target.value);
    if (!isPasswordMathing) {
      this.setState({color: '#FFC0CB'});
    } else {
      this.setState({color: ''});
    }
  }

  render() {
    return (<div className="input-group">
      <span className="input-group-addon">
        <i className="material-icons">lock</i>
      </span>
      <div className="form-line">
        <input style={{ backgroundColor: this.state.color }} type="password" className="form-control" name="password" minLength={6} placeholder="Confirmer le mot de passe" required="required" onChange={this.checkPassword}/>
      </div>
    </div>);
  }
}

export default InscriptionPasswordConfirmer;
