import React, {Component} from 'react';
import {connect} from 'react-redux';
import {fetchEntrepriseMusiques, fetchLoginMusique, deleteMusique} from '../../actions';
import {Link} from 'react-router-dom';
import _ from 'lodash';
import swal from 'sweetalert2';

class EntrepriseMusiques extends Component {

  componentDidMount() {
    window.closeMenu();
    this.props.fetchEntrepriseMusiques(this.props.user.id);
  }

  supprimerMusique(id){
    swal({
      title: 'Êtes-vous sûr?',
      text: 'Êtes-vous sûr de vouloir supprimer cette musique?',
      type: 'warning',
      showCancelButton: true,
      confirmButtonText: 'Oui',
      cancelButtonText: 'Non'
    }).then(() => {
      this.props.deleteMusique(id, () => {
        var playlistsKeys = _.keys(this.props.playlists);
        playlistsKeys.map(key => {
          this.props.fetchLoginMusique(this.props.playlists[key]);
        });
        swal(
          'Success!',
          'Votre musique a été supprimée!',
          'success'
        ).then(() => {
          this.props.history.push('/accueil');
        });
      });
    }, function(dismiss) {
      if (dismiss === 'cancel') {
      }
    });
  }


  renderMusique(musique) {
    return (
      <tr className="media row">
          <td className="col-xs-3"><h4 className="media-heading">{musique["titre"]}</h4></td>
          <td className="col-xs-3"><h4 className="media-heading">{musique["artiste"]}</h4></td>
          <td className="col-xs-2"><h4 className="media-heading">{musique["duree"]}</h4></td>
          <td className="col-xs-1"><h4 className="media-heading">{musique["is_visible"]}</h4></td>

          <td className="col-xs-1">
            <Link to={`/musique/${musique["id"]}`} className="btn btn-primary media-object" role="button">
              <i className="material-icons">visibility</i>
            </Link>
          </td>
        {/*  <td className="col-xs-1">
            <Link to="/editMusique" className="btn btn-primary waves-effect media-object" role="button">
              <i className="material-icons">mode_edit</i>
            </Link>
          </td>*/}
          <td className="col-xs-1">
            <a onClick={() => this.supprimerMusique(musique["id"])} href="javascript:void(0);" className="btn btn-primary waves-effect media-object">
              <i className="material-icons">delete</i>
            </a>
          </td>
      </tr>
    );
  }

  renderMusiques() {
    const {entrepriseMusiques} = this.props;
    return _.map(entrepriseMusiques, musique => {
      return (
        <table key={musique.id} className="media table table-striped">
          {this.renderMusique(musique)}
        </table>
      );
    });
  }

  render() {
    const {entrepriseMusiques} = this.props;
    if (typeof entrepriseMusiques === 'undefined' ) {
      return (<div>Loading</div>);
    }
    return (
      <div className="row clearfix">
        <div className="col-lg-12 col-md-12 col-sm-12 col-xs-12">
          <div className="card">
            <div className="body">
              <div className="bs-example" data-example-id="media-alignment">
                {this.renderMusiques()}
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

function mapStateToProps({ entrepriseMusiques, user, playlists }) {
  return { entrepriseMusiques, user, playlists};
}

export default connect(mapStateToProps, { fetchEntrepriseMusiques, fetchLoginMusique, deleteMusique })(EntrepriseMusiques);
