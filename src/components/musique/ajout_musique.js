import React, {Component} from 'react';
import { Field, reduxForm } from 'redux-form';
import { withRouter } from 'react-router-dom';
import { connect } from 'react-redux';
import Dropzone from 'react-dropzone';
import swal from 'sweetalert2'
import axios from 'axios';
import { ROOT_URL } from '../../actions';
const FILE_FIELD_NAME = 'file';

class AjoutMusique extends Component {

  componentDidMount() {
    window.closeMenu();
  }

  renderDropzoneInput(field){
    const files = field.input.value;
    const test = {};
    test.name="file";
    test.required=true;
    return (
      <div>
        <Dropzone
          name="file"
          accept="audio/mp3"
          multiple={false}
          maxFiles='1'
          inputProps={test}
          onDrop={( filesToUpload, e ) => field.input.onChange(filesToUpload)}
        >
        <div>Essayez des fichiers ici laissant tomber ou cliquez sur pour sélectionner les fichiers à télécharger.</div>
        </Dropzone>
        {field.meta.touched &&
          field.meta.error &&
          <span className="error">{field.meta.error}</span>}
        {files && Array.isArray(files) && (
          <ul>
            { files.map((file, i) => <li key={i}>{file.name}</li>) }
          </ul>
        )}
      </div>
    );
  }

  renderField(field){
    const { meta: { touched, error }} = field;
    return (
      <div className="input-group">
        <span className="input-group-addon">
          <i className="material-icons">{field.icon}</i>
        </span>
        <div className={`form-line ${error ? "style={{borderBottom: '2px solid #F44336'}}" : ''}`}>
          <input
            type={field.type}
            className="form-control"
            placeholder={field.label}
            autofocus={field.autofocus}
            name={field.name}
            style={ error ? {borderBottom: '2px solid #F44336'} : {}}
            {...field.input}
          />
        </div>
        <label id={`${field.name}-error`} className="error" for={field.name}>
          {touched ? error : ''}
        </label>
      </div>
    );
  }

  onSubmit(values){
    function checkStatus(response) {
      if (response.status >= 200 && response.status < 300) {
        return response
      } else {
        var error = new Error(response.statusText)
        error.response = response
        throw error
      }
    }

    swal({
      title: 'Êtes-vous sûr?',
      text: 'Est-ce que ces informations sont exactes!',
      type: 'warning',
      showCancelButton: true,
      confirmButtonText: 'Oui, ajoute!',
      cancelButtonText: 'Non, annuler'
    }).then(() => {
      const info = new FormData();
      info.append('titre', values.titre);
      info.append('artiste', values.artiste);
      info.append('duree', values.duree);
      info.append('idEntreprise', values.id);
      info.append('file', new Blob(values.file, { type: "audio/mp3" }));
      const request = axios({
        method: 'post',
        url:`${ROOT_URL}/file/`,
        data: info,
        config: { headers: {'Content-Type': 'multipart/form-data' }}
      });
      request.then(checkStatus).then((response) => {
          swal(
            'Success!',
            'Votre compte a été modifié.',
            'success'
          ).then(() => {
            this.props.history.push('/entrepriseMusiques');
          });
      }).catch(function(error) {
          swal('Oops...', 'Veuillez mettre au moins une durée à la musique', 'error');
      });
    }, function(dismiss) {
      // dismiss can be 'overlay', 'cancel', 'close', 'esc', 'timer'
      if (dismiss === 'cancel') {
      }
    })
  }

  render() {
    const { handleSubmit } = this.props;
    return (
      <div className="container-fluid">
        <div className="row clearfix">
          <div className="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div className="card">
              <div className="header"><h2>Ajouter une musique</h2></div>
              <div className="body">
                <form onSubmit={handleSubmit(this.onSubmit.bind(this))}>
                  <div className="row">
                    <div className="col-lg-8 col-md-8 col-sm-8 col-xs-8">
                      <Field
                        label="Titre de la musique"
                        icon="music_note"
                        type="text"
                        name="titre"
                        component={this.renderField}
                      />
                      <Field
                        label="L'artiste de la musique"
                        icon="person"
                        type="text"
                        name="artiste"
                        component={this.renderField}
                      />
                      <Field
                        label="Duree de la musique"
                        icon="timer"
                        type="text"
                        name="duree"
                        component={this.renderField}
                      />
                    </div>
                    <div className="col-lg-4 col-md-4 col-sm-4 col-xs-4">
                      <label htmlFor={FILE_FIELD_NAME}>Votre fichier MP3</label>
                      <Field
                        name="file"
                        component={this.renderDropzoneInput}
                      />
                    </div>
                  </div>
                  <input type="hidden" name="id" />
                  <button className="btn btn-primary waves-effect" type="submit">Ajouter la musique</button>
                </form>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

function validate(values){
  const errors = {};

  if (!values.titre && values.titre === '') {
    errors.titre = "Entrez un titre pour votre musique";
  }

  if (!values.artiste && values.artiste === '') {
    errors.artiste = "Entrez un artiste pour votre musique";
  }

  if (values.duree && !/^[0-9]+\.[0-9]{2}$/i.test(values.duree)) {
    errors.duree = "Entrez la durée de la musique. Example : 0.30 , 1.25 , 10.55";
  }

  return errors;
}

function mapStateToProps(state, ownProps){
  return ({
    initialValues: state.user
  });
}

AjoutMusique = reduxForm({
  validate,
  form: 'AjoutMusique'
})(AjoutMusique)

AjoutMusique = withRouter(connect(mapStateToProps)(AjoutMusique))

export default AjoutMusique;
