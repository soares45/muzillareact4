import React, {Component} from 'react';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';
import { setPlayerPlaylist, fetchAfficheMusique, fetchEntreprise, deleteMusique, fetchLoginMusique, findMyPlaylistIsNotHavingMusique, ajoutMusiqueInPlaylist } from '../../actions';
import swal from 'sweetalert2';
import axios from 'axios';
import _ from 'lodash';
import { ROOT_URL } from '../../actions';

class AfficheMusique extends Component {

  componentWillReceiveProps(nextProps){
    window.closeMenu();
    const nextId = nextProps.match.params.id;
    const prevId = this.props.match.params.id;
    if( nextId !== prevId ){
      const { id } = nextProps.match.params;
      this.props.fetchAfficheMusique(id);
      this.props.fetchEntreprise(id);
      this.props.findMyPlaylistIsNotHavingMusique(this.props.user.id, id);
    }
  }

  componentDidMount(){
    window.closeMenu();
    const { id } = this.props.match.params;
    this.props.fetchAfficheMusique(id);
    this.props.fetchEntreprise(id);
    this.props.findMyPlaylistIsNotHavingMusique(this.props.user.id, id);
  }

  ajouterPlaylist(idMusique) {
    swal({
      title: 'Ajouter la musique à une playlist',
      input: 'select',
      type: 'question',
      inputOptions: this.props.playlistNotMusique,
      inputPlaceholder: 'Choisissez votre playlist',
      showCancelButton: true,
      confirmButtonText: 'Ajouter',
      cancelButtonText: 'Annuler',
      showLoaderOnConfirm: true
    }).then((value) => {
      if (value) {
        this.props.ajoutMusiqueInPlaylist(idMusique, value);
        swal('Votre musique a été ajoutée dans votre playlist');
      }
    });
  }

  ajouterAHistorique(idMusique) {
    var utilisateurId = this.props.user.id;
    axios.get(`${ROOT_URL}/client/findMyHistoryPlaylist/${utilisateurId}`).then( (response) => {
      console.log(response.data);
      this.props.ajoutMusiqueInPlaylist(idMusique, response.data);
    });
  }

  supprimerMusique(idMusique){
    swal({
      title: 'Êtes-vous sûr?',
      text: 'Êtes-vous sûr de vouloir supprimer cette musique?',
      type: 'warning',
      showCancelButton: true,
      confirmButtonText: 'Oui',
      cancelButtonText: 'Non'
    }).then(() => {
      this.props.deleteMusique(idMusique, () => {
        var playlistsKeys = _.keys(this.props.playlists);
        playlistsKeys.map(key => {
          this.props.fetchLoginMusique(this.props.playlists[key]);
        });
        swal(
          'Success!',
          'Votre musique a été supprimée!',
          'success'
        ).then(() => {
          this.props.history.push('/accueil');
        });
      });
    }, function(dismiss) {
      if (dismiss === 'cancel') {
      }
    });
  }

  render(){
    const {musiqueActive, entrepriseActive} = this.props;
    if(!this.props.musiqueActive){
      return <div>Aucune musique valide sélectionnée</div>;
    }
    var editMusique_menu = '';
    var supprimerMusique_menu = '';
    if(this.props.user.type === 'entreprise' && this.props.user.id === this.props.musiqueActive.entrepriseId){
      editMusique_menu = (
        <li>
          <Link to="/editMusique">
            <i className="material-icons">mode_edit</i>
            <span>Modifier</span>
          </Link>
        </li>
      );
      supprimerMusique_menu = (
        <li>
          <a onClick={() => this.supprimerMusique(musiqueActive.id)} href="javascript:void(0);">
            <i className="material-icons">delete</i>
            Supprimer
          </a>
        </li>
      );
    }
    var musique_playlist = [];
    musique_playlist.push(musiqueActive);
    console.log(musique_playlist);
    return (
      <div className="card">
        <div className="header">
          <h2>
            {musiqueActive.titre} <br/>
            <small>Par {musiqueActive.artiste}</small>
          </h2>
          <ul className="header-dropdown m-r--5">
            <li className="dropdown">
              <a href="javascript:void(0);" className="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                <i className="material-icons">more_vert</i>
              </a>
              <ul className="dropdown-menu pull-right">
                <li>
                  <a onClick={() => this.props.setPlayerPlaylist(musique_playlist)} href="javascript:void(0);">
                    <i className="material-icons">play_arrow</i>
                    &Eacute;couter
                  </a>
                </li>
                {editMusique_menu}
                <li>
                  <a onClick={() => this.ajouterPlaylist(musiqueActive.id)} href="javascript:void(0);">
                    <i className="material-icons">library_add</i>
                    Ajouter a playlist
                  </a>
                </li>
                {supprimerMusique_menu}
              </ul>
          </li>
        </ul>
      </div>
      <div className="body">
        <p>Boite de production: {entrepriseActive.username}</p>
        <p>Durée: {musiqueActive.duree}</p>
        <p>Piste: {musiqueActive.piste}</p>
      </div>
    </div>
    );
  }
}

function mapStateToProps({ musiqueActive, entrepriseActive, playlists, user, playlistNotMusique }){
  return { musiqueActive, entrepriseActive, playlists, user, playlistNotMusique };
}

export default connect(mapStateToProps, { setPlayerPlaylist, fetchAfficheMusique, fetchEntreprise, deleteMusique, fetchLoginMusique, findMyPlaylistIsNotHavingMusique, ajoutMusiqueInPlaylist } )(AfficheMusique);
