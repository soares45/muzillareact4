import React, {Component} from 'react';
import { Field, reduxForm } from 'redux-form';
import { connect } from 'react-redux';
import { updateMusique ,fetchLoginMusique} from '../../actions';
import swal from 'sweetalert2';
import axios from 'axios';
import Dropzone from 'react-dropzone';
import { ROOT_URL } from '../../actions';
import _ from 'lodash';
const FILE_FIELD_NAME = 'file';


class EditMusique extends Component {

  componentDidMount() {
    window.closeMenu();
  }

  renderDropzoneInput(field){
    const files = field.input.value;
    const test = {};
    test.name="file";
    test.required=true;
    return (
      <div>
        <Dropzone
          name="file"
          accept="audio/mp3"
          multiple={false}
          maxFiles='1'
          inputProps={test}
          onDrop={( filesToUpload, e ) => field.input.onChange(filesToUpload)}
        >
        <div>Glissez un fichier ici ou cliquez sur pour sélectionner les fichiers à télécharger.</div>
        </Dropzone>
        {field.meta.touched &&
          field.meta.error &&
          <span className="error">{field.meta.error}</span>}
        {files && Array.isArray(files) && (
          <ul>
            { files.map((file, i) => <li key={i}>{file.name}</li>) }
          </ul>
        )}
      </div>
    );
  }


  renderField(field){
    const { meta: { touched, error }} = field;
    return (
      <div className="input-group">
        <span className="input-group-addon">
          <i className="material-icons">{field.icon}</i>
        </span>
        <div className={`form-line ${error ? "style={{borderBottom: '2px solid #F44336'}}" : ''}`}>
          <input
            type={field.type}
            className="form-control"
            placeholder={field.label}
            autofocus={field.autofocus}
            name={field.name}
            style={ error ? {borderBottom: '2px solid #F44336'} : {}}
            {...field.input}
          />
        </div>
        <label id={`${field.name}-error`} className="error" for={field.name}>
          {touched ? error : ''}
        </label>
      </div>
    );
  }

  onSubmit(values){
    swal({
      title: 'Êtes-vous sûr?',
      text: 'Les modifications seront permanentes!',
      type: 'warning',
      showCancelButton: true,
      confirmButtonText: 'Oui, enregister!',
      cancelButtonText: 'Non, annuler'
    }).then(() => {

      const info = new FormData();
      info.append('titre', values.titre);
      info.append('artiste', values.artiste);
      info.append('duree', values.duree);
      info.append('idEntreprise', this.props.user.id);
      info.append('id', values.id)
      info.append('file', new Blob(values.file, { type: "audio/mp3" }));
      const request = axios({
        method: 'post',
        url:`${ROOT_URL}/file/update/`,
        data: info,
        config: { headers: {'Content-Type': 'multipart/form-data' }}
      });
      const data = request.then((response) => {
        if('Téléversement Réussi' === data){

            var playlistsKeys = _.keys(this.props.playlists);
            playlistsKeys.map(key => {
              this.props.fetchLoginMusique(this.props.playlists[key]);
            });
            swal(
              'Success!',
              'La musique a été modifiée.',
              'success'
            ).then(() => {
              this.props.history.push(`/musique/${values.id}`);
            });
          }
      })
    }, function(dismiss) {
      // dismiss can be 'overlay', 'cancel', 'close', 'esc', 'timer'
      if (dismiss === 'cancel') {
      }
    })
  }

  render() {
    const { handleSubmit } = this.props;
    return (
      <div className="container-fluid">
        <div className="row clearfix">
          <div className="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div className="card">
              <div className="header"><h2>Modifications de la musique</h2></div>
              <div className="body">
                <form onSubmit={handleSubmit(this.onSubmit.bind(this))}>
                <div className="row">
                  <div className="col-lg-8 col-md-8 col-sm-8 col-xs-8">
                  <Field
                    label="Titre"
                    icon="title"
                    type="text"
                    name="titre"
                    component={this.renderField}
                  />
                  <Field
                    label="Artiste"
                    icon="person"
                    type="text"
                    name="artiste"
                    component={this.renderField}
                  />
                  </div>
                  <div className="col-lg-4 col-md-4 col-sm-4 col-xs-4">
                    <label htmlFor={FILE_FIELD_NAME}>Votre fichier MP3</label>
                    <Field
                      name="file"
                      component={this.renderDropzoneInput}
                    />
                  </div>
                  </div>
                  <input type="hidden" name="id" />
                  <button className="btn btn-primary waves-effect" type="submit">Modifier</button>
                </form>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

function validate(values){
  const errors = {};
  if (null !== values.titre && values.titre === '') {
    errors.titre = "Entrez un titre pour cette musique";
  }
  if (null !== values.artiste && values.artiste === '') {
    errors.artiste = "Entrez un artiste pour cette musique";
  }
  return errors;
}

function mapStateToProps(state, ownProps){
  return ({
    initialValues: state.musiqueActive,
    playlists: state.playlists,
    user: state.user
  });
}

EditMusique = reduxForm({
  validate,
  form: 'EditMusique'
})(EditMusique)

EditMusique = connect(mapStateToProps,{updateMusique, fetchLoginMusique})(EditMusique)

export default EditMusique;
