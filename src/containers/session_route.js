import React, { Component } from 'react';
import { connect } from 'react-redux';
import {Route, Switch} from 'react-router-dom';
import Accueil from '../components/accueil';
import Login from '../components/login';
import Inscription from '../components/inscription';
import Presentation from '../components/presentation';

class SessionRoute extends Component {

  render() {
    const { user } = this.props;
    if (user && user.id) {
      return (
        <Switch>
          <Route path="/" component={Accueil} />
        </Switch>
      );
    } else {
      return (
        <Switch>
          <Route exact path="/inscription" component={Inscription} />
          <Route exact path="/connection" component={Login} />
          <Route exact path="/" component={Presentation} />
          <Route component={Presentation}/>
        </Switch>
      );
    }
  }
}

function mapStateToProps({user}){
  return {user};
}

export default connect(mapStateToProps)(SessionRoute);
