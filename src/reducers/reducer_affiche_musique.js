import _ from 'lodash';
import { FETCH_AFFICHE_MUSIQUE } from '../actions';

export default function(state = {}, action){
  switch (action.type) {
    case FETCH_AFFICHE_MUSIQUE:
      return action.payload.data;
      break;
    default:
      return state;
  }
}
