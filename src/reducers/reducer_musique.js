import _ from 'lodash';
import { FETCH_LOGIN_MUSIQUE } from '../actions';

export default function(state = {}, action){
  switch (action.type) {
    case FETCH_LOGIN_MUSIQUE:
      if (typeof action.payload.musique !== 'undefined') {
        return _.merge(action.payload.musique, _.omit(state, _.keys(action.payload.musique)));
      }
      return _.merge({}, state);
    default:
      return state;
  }
}
