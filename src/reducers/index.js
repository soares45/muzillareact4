import { combineReducers } from 'redux';
import { reducer as formReducer } from 'redux-form';
import UtilisateurSession from './reducer_session';
import PlaylistReducer from './reducer_playlist';
import PlaylistActiveReducer from './reducer_playlist_active';
import PlaylistIsNotInsideMusique from './playlist_not_musique';
import MusiqueReducer from './reducer_musique';
import MusiquesActiveReducer from './reducer_musiques_actives';
import AfficheMusiqueReducer from './reducer_affiche_musique';
import LecteurMusiqueReducer from './reducer_lecteur';
import AfficheEntrepriseActiveReducer from './reducer_entreprise_active';
import EntrepriseMusiquesReducer from './reducer_entreprise_musiques';
import PlayerPlaylistReducer from './reducer_player_playlist';
import MusiquesAleatoires from './reducer_musiques_aleatoires';
import RechercheReducer from './reducer_recherche';


const rootReducer = combineReducers({
  user: UtilisateurSession,
  form: formReducer,
  playlists: PlaylistReducer,
  playlistActif: PlaylistActiveReducer,
  playlistNotMusique: PlaylistIsNotInsideMusique,
  musiques: MusiqueReducer,
  musiquesActives: MusiquesActiveReducer,
  musiquesAleatoires: MusiquesAleatoires,
  musiqueActive: AfficheMusiqueReducer,
  LecteurMusique: LecteurMusiqueReducer,
  entrepriseActive: AfficheEntrepriseActiveReducer,
  entrepriseMusiques: EntrepriseMusiquesReducer,
  playerPlaylist: PlayerPlaylistReducer,
  resultats: RechercheReducer
});

export default rootReducer;
