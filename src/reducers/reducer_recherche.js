import {FETCH_RECHERCHE, CLEAR_RECHERCHE} from '../actions';

export default function(state = {}, action) {
  switch (action.type) {
    case FETCH_RECHERCHE:
      return action.payload.data;
      break;
    case CLEAR_RECHERCHE:
      return [];
    default:
      return [];
  }
}
