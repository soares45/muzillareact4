import _ from 'lodash';
import {FETCH_LOGIN_PLAYLIST, AJOUT_PLAYLIST, UPDATE_PLAYLIST, SUPPRIMER_PLAYLIST, CLONE_PLAYLIST} from '../actions';

export default function(state = {}, action) {
  switch (action.type) {
    case FETCH_LOGIN_PLAYLIST:
      return _.mapKeys(action.payload.data, 'id');
      break;
    case AJOUT_PLAYLIST:
      return _.mapKeys(action.payload.data, 'id');
      break;
    case UPDATE_PLAYLIST:
      return _.mapKeys(action.payload.data, 'id');
      break;
    case SUPPRIMER_PLAYLIST:
      return _.mapKeys(action.payload.data, 'id');
      break;
    case CLONE_PLAYLIST:
      return _.mapKeys(action.payload.data, 'id');
      break;
    default:
      return state;
  }
}
