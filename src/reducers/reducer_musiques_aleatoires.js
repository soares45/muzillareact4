import _ from 'lodash';
import { MUSIQUE_ALEATOIRE  } from '../actions';

export default function(state = {}, action){
  switch (action.type) {
    case MUSIQUE_ALEATOIRE:
      return _.mapKeys(action.payload.data, 'id');
      break;
    default:
      return state;
  }
}
