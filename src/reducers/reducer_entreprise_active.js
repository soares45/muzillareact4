import { FETCH_ENTREPRISE_ACTIVE } from '../actions';

export default function(state = {}, action){
  switch (action.type) {
    case FETCH_ENTREPRISE_ACTIVE:
      return action.payload.data;
      break;
    default:
      return state;
  }
}
