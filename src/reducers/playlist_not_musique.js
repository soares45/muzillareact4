import {PLAYLIST_NOT_MUSIQUE} from '../actions';

export default function(state = {}, action) {
  switch (action.type) {
    case PLAYLIST_NOT_MUSIQUE:
      return action.payload.data;
      break;
    default:
      return state;
  }
}
