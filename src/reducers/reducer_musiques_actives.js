import _ from 'lodash';
import { FETCH_MUSIQUE_PLAYLIST  } from '../actions';

export default function(state = {}, action){
  switch (action.type) {
    case FETCH_MUSIQUE_PLAYLIST:
      return _.mapKeys(action.payload.data, 'id');
    default:
      return state;
  }
}
