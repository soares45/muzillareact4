import _ from 'lodash';
import { FETCH_ENTREPRISE_MUSIQUES  } from '../actions';

export default function(state = {}, action){
  switch (action.type) {
    case FETCH_ENTREPRISE_MUSIQUES:
      return _.mapKeys(action.payload.data, 'id');
      break;
    default:
      return state;
  }
}
