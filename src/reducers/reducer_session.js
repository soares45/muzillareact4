import { FETCH_LOGIN, FETCH_UPDATE_PROFILE, CONVERT_CLIENT } from '../actions';

export default function(state = {}, action){
  switch (action.type) {
    case FETCH_LOGIN:
      return action.payload.data;
    break;
    case FETCH_UPDATE_PROFILE:
      return action.payload.data;
    break;
    case CONVERT_CLIENT:
      return action.payload.data;
    }
  return state;
}
