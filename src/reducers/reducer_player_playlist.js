import {PLAYER_PLAYLIST} from '../actions';

export default function(state = {}, action) {
  switch (action.type) {
    case PLAYER_PLAYLIST:
      return action.payload;
      break;
    default:
      return state;
  }
}
