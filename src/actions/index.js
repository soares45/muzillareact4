import axios from 'axios';
import swal from 'sweetalert2';

export const FETCH_LOGIN = 'fetch_login';
export const FETCH_UPDATE_PROFILE = 'fetch_update_profile';
export const FETCH_LOGIN_PLAYLIST = 'fetch_login_playlist';
export const AJOUT_PLAYLIST = 'ajout_playlist';
export const FETCH_MUSIQUE_PLAYLIST = 'fetch_musique_playlist';
export const FETCH_PLAYLIST = 'fetch_playlist';
export const UPDATE_PLAYLIST = 'update_playlist';
export const SUPPRIMER_PLAYLIST = 'supprimer_playlist';
export const CLONE_PLAYLIST = 'clone_playlist';
export const FETCH_LOGIN_MUSIQUE = 'fetch_login_musique';
export const PLAYLIST_NOT_MUSIQUE = 'playlist_not_musique';
export const AJOUT_MUSIQUE_LECTEUR = 'ajout_musique_lecteur';
export const AJOUT_MUSIQUE_PLAYLIST = 'ajout_musique_playlist';
export const FETCH_AFFICHE_MUSIQUE = 'fetch_affiche_musique';
export const UPDATE_MUSIQUE = 'update_musique';
export const DELETE_MUSIQUE = 'delete_musique';
export const RETIRER_MUSIQUE = 'delete_musique';
export const CONVERT_CLIENT = 'convert_client';
export const FETCH_ENTREPRISE_ACTIVE = 'fetch_entreprise';
export const PLAYER_PLAYLIST = 'player_playlist';
export const MUSIQUE_ALEATOIRE = 'musique_aleatoire';
export const FETCH_RECHERCHE= 'fetch_recherche';
export const CLEAR_RECHERCHE= 'clear_recherche';


export const FETCH_ENTREPRISE_MUSIQUES = 'fetchEntrepriseMusiques';

export const ROOT_URL = 'http://localhost:8094';

/* User actions */
export function setPlayerPlaylist(musiques){
  var playlist = [];
  musiques.forEach(musique => (playlist.push(musique.piste)));

  return {
    type: PLAYER_PLAYLIST,
    payload: playlist
  }
}

export function convertirClient(id, callback){
  const request = axios.get(`${ROOT_URL}/entreprise/convert/${id}`);
  request.then(() => callback());

  return {
    type: CONVERT_CLIENT,
    payload: request
  }
}
export function fetchLogin(values, callback){
  const request = axios.post(`${ROOT_URL}/entreprise/login`, values);
  request.then(() => callback());
  return {
    type: FETCH_LOGIN,
    payload: request
  }
}

export function fetchUpdateProfile(values, callback){
  function checkStatus(response) {
    if (response.status >= 200 && response.status < 300) {
      return response
    } else {
      var error = new Error(response.statusText)
      error.response = response
      throw error
    }
  }

  delete values.password;
  if (typeof values.newPassword !== 'undefined') {
    values.password = values.newPassword;
    values.flag = 'true';
  } else {
    values.flag = 'false';
  }
  const request = axios.post(`${ROOT_URL}/client/updateClient/${values.flag}`, values);
  request.then(checkStatus).then(() => callback()).catch(function(error) {
      swal('Oops...', 'L\'utilisateur ou l\'email existe déjà. Veuillez changer de nom ou d\'email.', 'error');
  });

  return {
    type: FETCH_UPDATE_PROFILE,
    payload: request
  }
}

/* Music actions */

export function fetchLoginMusique(playlist){
  const playlistId = (playlist.playlistId ? playlist.playlistId : playlist.id );
  const request = axios.get(`${ROOT_URL}/musique/findByPlaylist/${playlistId}`);
  const data = request.then(function (response) {
    const musique = {};
    const musiques = {};
    musique[''+playlistId] = response.data;
    musiques.musique = musique;
    return musiques;
  })
  return {
    type: FETCH_LOGIN_MUSIQUE,
    payload: data
  }
}

export function ajoutMusiqueLecteur(musique){
  return {
    type: AJOUT_MUSIQUE_LECTEUR,
    payload: musique
  }
}

export function fetchAfficheMusique(id){
  const request = axios.get(`${ROOT_URL}/musique/${id}`);

  return {
    type: FETCH_AFFICHE_MUSIQUE,
    payload: request
  }
}

export function fetchMusiqueByPlaylistId(id){
  const request = axios.get(`${ROOT_URL}/musique/findByPlaylist/${id}`);
  return {
    type: FETCH_MUSIQUE_PLAYLIST,
    payload: request
  }
}

export function updateMusique(values, callback){
  const request =  axios.post(`${ROOT_URL}/musique/updateMusique/`, values);
  request.then(() => callback());

  return {
    type: UPDATE_MUSIQUE,
    payload: request
  }
}

export function deleteMusique(id, callback){
  const request = axios.get(`${ROOT_URL}/musique/deleteById/${id}`);
  request.then(() => callback());
  return {
    type: DELETE_MUSIQUE,
    payload: request
  }
}

/* Playlist actions */

export function fetchLoginPlaylist(user){
  const request = axios.get(`${ROOT_URL}/playlist/findByClient/${user.id}`);

  return {
    type: FETCH_LOGIN_PLAYLIST,
    payload: request
  }
}

export function findMyPlaylistIsNotHavingMusique(idClient, idMusique){
  const request = axios.get(`${ROOT_URL}/client/findMyPlaylistIsNotHavingMusique/${idClient}/${idMusique}`);

  return {
    type: PLAYLIST_NOT_MUSIQUE,
    payload: request
  }
}

export function ajoutMusiqueInPlaylist(idMusique, idPlaylist){
  const request = axios.get(`${ROOT_URL}/musique/ajoutMusiqueInPlaylist/${idMusique}/${idPlaylist}`);

  return {
    type: AJOUT_MUSIQUE_PLAYLIST,
    payload: request
  }
}

export function fetchPlaylist(id){
  const request = axios.get(`${ROOT_URL}/playlist/findById/${id}`);
  console.log("request playlists " + request);

  return {
    type: FETCH_PLAYLIST,
    payload: request
  }
}

export function ajoutPlaylist(values, id){
  const request = axios.post(`${ROOT_URL}/playlist/createPlaylist/${id}`, values);
  return {
    type: AJOUT_PLAYLIST,
    payload: request
  }
}

export function updatePlaylist(values, id, callback){
  const request =  axios.post(`${ROOT_URL}/playlist/updatePlaylist/${id}`, values);
  request.then(() => callback());

  return {
    type: UPDATE_PLAYLIST,
    payload: request
  }
}

export function supprimerPlaylist(id, callback){
  const request =  axios.get(`${ROOT_URL}/playlist/deleteById/${id}`);
  request.then(() => callback());
  return {
    type: SUPPRIMER_PLAYLIST,
    payload: request
  }
}

export function clonePlaylist(values, id, callback){
  const request =  axios.post(`${ROOT_URL}/playlist/clonePlaylist/${id}`, values);
  request.then(() => callback());
  return {
    type: CLONE_PLAYLIST,
    payload: request
  }
}

export function retireMusique(idMusique, idPlaylist, callback){
  const request = axios.get(`${ROOT_URL}/musique/retireMusiqueInPlaylist/${idMusique}/${idPlaylist}`);
  request.then(() => callback());
  return {
    type: RETIRER_MUSIQUE,
    payload: request
  }
}

export function fetchMusiqueAleatoire(){
  const request = axios.get(`${ROOT_URL}/musique/findMusiqueAleatoire`);

  return {
    type: MUSIQUE_ALEATOIRE,
    payload: request
  }
}

          /* Entreprise actions */
export function fetchEntreprise(id){
  const request = axios.get(`${ROOT_URL}/entreprise/findByMusique/${id}`);

  return {
    type: FETCH_ENTREPRISE_ACTIVE,
    payload: request
  }
}

export function fetchEntrepriseMusiques(id){
  const request = axios.get(`${ROOT_URL}/entreprise/getMusiquesByEntrepriseId/${id}`);

  return {
    type: FETCH_ENTREPRISE_MUSIQUES,
    payload: request
  }
}

export function fetchRecherche(term){
  const request = axios.get(`${ROOT_URL}/client/recherche/${term}`);
  return {
    type: FETCH_RECHERCHE,
    payload: request
  }
}
export function clearRecherche(){
  return {
    type: CLEAR_RECHERCHE,
    payload: {}
  }
}
