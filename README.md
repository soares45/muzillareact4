# MuzillaReact

## Synopsis

Frontend du projet Muzilla

## Installation

### Clone

```
git clone https://flacoursiere@bitbucket.org/Muzilla/muzillareact.git
```

### Commandes
Se deplacer dans le projet
```
cd muzillareact
```

#### Installation des dependences

```
npm install
```

#### Demarrage du serveur React

```
npm start
```

[Assurer vous d'avoir demarer votre server sprint boot](https://bitbucket.org/Muzilla/muzilla)
#### Se connecter au serveur local avec le navigateur

[Click me](http://localhost:8080)

`http://localhost:8080`

## Contributors

* Alexandre Soares
* Rafael David Higuera Silva
* Richard Diamond
* Aurelie Leao
