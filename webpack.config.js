"use strict";
var path = require('path');
const paths = {
  DIST: path.resolve(__dirname, 'dist'),
  JS: path.resolve(__dirname, 'src/js'),
};
const HOST = process.env.HOST || "127.0.0.1";
const PORT = process.env.PORT || "8080";
module.exports = {
  entry: [
    './src/index.js'
  ],
  output: {
    path: paths.DIST,
    publicPath: '/',
    filename: 'bundle.js'
  },
  module: {
    loaders: [{
      exclude: /node_modules/,
      loader: 'babel-loader',
      query: {
        presets: ['react', 'es2015', 'stage-1']
      }
    }]
  },
  resolve: {
    extensions: ['.js', '.jsx']
  },
  devServer: {
    historyApiFallback: true,
    contentBase: './',
    port: PORT
  }
};
